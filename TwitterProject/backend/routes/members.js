const express = require('express');
const router = express.Router();

const sql = require("../db");

const Joi = require('joi');
const { valid } = require('joi');
const validator = require('express-joi-validation').createValidator({ pathError: true })

/* TODO: Add Joi validation */

/* TODO: Add edit function once you get the list to display */

/* GET all members in DB */
router.get('/', function(req, res, next) {
  sql.query('SELECT * from members', function(error, results, fields){
      if (error) throw error;
      res.json(results);
  })
})

/* POST add member to db */
/* FIXME Figure out why some emojis aren't accepted */
router.post('/add/', function(req, res, next) {
  sql.query('INSERT into members SET ?', req.body, (error, result) => {
    if (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        res.status(400).send({
          message : "Already exists. try again"
        })
      }
      next(error)
    }
    res.json(result);
  })
});

module.exports = router;
