const express = require('express');
const router = express.Router();

const sql = require("../db");

const Joi = require('joi');
const { valid } = require('joi');
const { response } = require('express');
const validator = require('express-joi-validation').createValidator({ pathError: true })


/* TODO: add join validation to POST */

/* TODO: add edit function if you can get the list calling properly */

/* use
 select * from sets JOIN memberset ON sets.membersetid=memberset.membersetid; 
 if needed */
/* GET sets list */
router.get('/', function(req, res, next) {
    sql.query('SELECT * from sets', function(error, results, fields){
        if (error) throw error;
        res.json(results);
    })
})

/* GET member set list */
router.get('/membersets/', function(req, res, next) {
    sql.query('SELECT * from memberset', function(error, results, fields){
        if (error) throw error;
        res.json(results);
    })
})

/* POST add member set */
router.post('/add/', function(req, res, next) {
    const query = req.body;

    fetch(`https://api.twitter.com/1.1/collections/create.json?name=${req.body.setname}`, {
        method: 'POST',
        headers: {
            'Accept': "application/json",
            'Content-Type': 'application/json',
            'authorization': 'INSERT AUTHORIZATION HERE'
        }
    }).then(response => response.json())
    .then((json) => {
        query[collectionid] = json.timeline_id
    })

    sql.query('INSERT into sets SET ?', query, (error, result) => {
      if (error) {
        if (error.code === 'ER_DUP_ENTRY') {
          res.status(400).send({
            message : "Already exists. try again"
          })
        }
        next(error)
      }
      res.json(result);
    })
  });

module.exports = router;