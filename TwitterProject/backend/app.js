const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');

const indexRouter = require('./routes/index');
const membersRouter = require('./routes/members');
const setsRouter = require('./routes/sets');

const app = express();

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use('/', indexRouter);
app.use('/members', membersRouter);
app.use('/sets', setsRouter);

app.use((err, req, resp, next) =>{
    console.error(err);

    //check for joi errors
    if (err && err.error && err.error.isJoi){
        resp.status(400).json({
            message : err.error.toString(),
            error : err.message
        });
    }else{
        
        if (!resp){
            resp.status(500).json({
                message : "something went wrong",
                error : err.message
            })
    
        }
    }
})


module.exports = app;
