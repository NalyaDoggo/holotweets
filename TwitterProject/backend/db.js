'use strict';

const mysql = require('mysql');

//local mysql db connections
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'Jac12345*',
    database: 'holotweetsdb'
});

connection.connect(function(err) {
    if (err) throw err;
    console.log("DB connection successful");
});

module.exports = connection;
