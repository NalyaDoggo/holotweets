import React from 'react';

class MembersList extends React.Component {

    constructor() {
        super();

        this.state = { 
            membersList: [],
            isButtonPressed: false
        }
        this.getList = this.getList.bind(this);
    }

    /**
     * loads the state variable membersList with the db table members via the getList() function below.
     * Uses a GET request. For some reason, doesn't work with the image in place.
     */

    componentDidMount() {
        this.getList();
    }

    
    getList() {
        this.setState({isButtonPressed: true});

        fetch('http://localhost:3001/members', {
          method: 'GET',
          headers: {
            Accept: "application/json"
            }
          })
          .then((response) => response.json())
          .then((json) => {
              /* FIXME: memberList isn't accepted here. An object with index values is returned but it's not accepted as a React Object.
              Need to fix the image transfering somehow */
            this.setState({
                membersList: json, 
                isButtonPressed: false
            })
        });
      }

    /**
     * a function that iterates through the state memberlist to populate multiple html elements
     * @returns returns the member list in the form of html elements 
     */

    /* Consider replacing the image section with an actual <img> with src being item.imagefilepath instead of the actual image */
    renderList() {
    return (
        <div>
            {this.state.membersList.map(item => 
                <tr key={item.memberid} >
                    <td>{item.image}</td>
                    <td>{item.name}</td>
                </tr>)}
        </div>
        );
    }
    
    
    render() {
        return (
            <div className="MainBody">
                <table>
                    <tr>
                        <th>Member Image</th>
                        <th>Member Name</th>
                    </tr>
                    {this.isButtonPressed ? 'Getting member list...' : this.renderList()}
                </table>
            </div>
        )
    }
    
}


export default MembersList

