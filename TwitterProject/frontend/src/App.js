
import React from 'react';
import {BrowserRouter as Router, Link, Switch, Route } from 'react-router-dom';
import Manage from './manage';
import Members from './members';
import MembersList from './listmember';
import Home from './Home';
import './app.css';


class App extends React.Component {

  constructor() {
    super();
    this.state = {
      
      memberid: "",
      name: "",
      image: null,
      fanmark: "",

      isSubmitting: false,

      /**
       * Routes for the navbar
       */
      routes: [
        {
          path: "/",
          exact: true,
          topbar: () => <div className="Header">Home</div>,
          main: () => <Home/>
        },
        {
          path: "/manage",
          exact: true,
          topbar: () => <div className="Header">Manage Page</div>,
          main: () => <Manage/>
        },
        {
          path: "/addmembers",
          exact: true,
          topbar: () => <div className="Header">Add Member</div>,
          main: () => <Members 
            onFormSubmit={this.addMember}
            onChange={this.onChange}
            />
        },
        {
          path: "/memberlist",
          exact: true,
          topbar: () => <div className="Header">Members List</div>,
          main: () => <MembersList/>
        }
      ]
    };
    this.addMember = this.addMember.bind(this);
    this.onChange = this.onChange.bind(this);
  };

  /**
   * Adds a member to the database using the state as the source of data
   * is called in Members.js
   */
  addMember() {
    this.setState({isSubmitting:true});

    fetch('http://localhost:3001/members/add', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({memberid: this.state.memberid,
        name: this.state.name,
        image: this.state.image,
        fanmark: this.state.fanmark})
    });
    /* TODO: Add error handling and proper */

    this.setState({isSubmitting:false});
  }

  onChange(e) {
    this.setState({
      [e.target.id]: e.target.value
    })
  }
  
  render() {
    return (
      <div className="App">
        <Router>
          <div className="NavBar">
            <Link to="/">Home</Link>
            <Link to="/manage">Manage Groups</Link>
            <Link to="/addmembers">Add Members</Link>
            <Link to="/memberlist">Members List</Link>
          </div>
          <Switch>
          {this.state.routes.map((route, index) => (
              <Route 
              key={index}
              path={route.path}
              exact={route.exact}
              children={<route.topbar/>}
              />
          ))}
          </Switch>
          <div className="MainBody">
              <Switch>
                  {this.state.routes.map((route, index) => (
                      <Route 
                      key={index}
                      path={route.path}
                      exact={route.exact}
                      children={<route.main/>}
                      />
                  ))}
              </Switch>
          </div>
          
        </Router>
      </div>
    );
  }
}

export default App;
