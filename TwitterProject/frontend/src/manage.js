import React from 'react';
import './manage.css';

class Manage extends React.Component {
    constructor() {
    super();
    this.state = {
        isNewSet:false,
        setList:[],
        memberSetList:[],
        selectedSet: "",
        hashtag: "",
        setname: "",
        isSubmitting:false
        }
        this.onCreateSet = this.onCreateSet.bind(this);
        this.onChange = this.onChange.bind(this);
        this.addSet = this.addSet.bind(this);
        this.renderNewSet = this.renderNewSet.bind(this);
        this.renderSetList = this.renderNewSet.bind(this);
        this.onGoBack = this.onGoBack.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    /**
     * calls the functions getSetList() and getMemberSetList() on load to populate the states setList and memberSetList with data from the db.
     */
    componentDidMount() {
        this.getSetList();
        this.getMemberSetList();
    }

    /**
     * GET request to localhost:3001/sets
     * should get the response back that is the db data for setList
     */
    getSetList() {

        fetch('http://localhost:3001/sets', {
          method: 'GET',
          headers: {
            Accept: "application/json"
            }
          })
          .then((response) => response.json())
          .then((json) => {
            this.setState({
                setList: json
            })
        });
      }

    getMemberSetList() {
    fetch('http://localhost:3001/sets/membersets', {
        method: 'GET',
        headers: {
            Accept: "application/json"
            }
        })
        .then((response) => response.json())
        .then((json) => {
            this.setState({
                memberSetList: json
            })
        });
    }
      
      /* FIXME: This is apparently undefined when called so... Find out what's wrong */ 
    onCreateSet() {
        this.setState({isNewSet: true});
    }
      
    onGoBack() {
        this.setState({isNewSet: false});
    }

    renderSetList() {
        console.log("RenderSetList");
        return(
            <div className="SetList">
                {this.state.setList.map(item => 
                <div className="Set" key={item.setid}>
                    <h4>{item.setname}</h4>
                    <p>{item.hashtag}</p>
                </div>)}
                <div>
                    <button onClick={this.onCreateSet}>Create new set</button>
                </div>
            </div>
        )
    }

      onChange(e) {
        this.setState({
          [e.target.id]: e.target.value
        })
      }

    addSet() {
        this.setState({isSubmitting:true});

        fetch('http://localhost:3001/sets/add', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({selectedSet: this.state.selectedSet,
            hashtag: this.state.hashtag,
            setname: this.state.setname})
        });
        /* TODO: Add error handling and proper */

        this.setState({isSubmitting:false});
        this.setState({isNewSet: false});
    }   

     onSubmit(event) {
        event.preventDefault();
        this.addSet();
    }

    renderNewSet() {
        console.log("renderNewSet");
        return(
            <div className="NewSet">
                <form method="POST" action="http://localhost:3001/sets/add" onSubmit={this.onSubmit}>
                    <label>
                        Member Set 
                        <select id="memberid" value={this.state.selectedSet} onChange={(e) => this.setState({selectedTeam: e.target.value})}>
                            {this.state.memberSetList.map(item=>
                            <option key={item.membersetid}>{item.membersetname}</option>)}
                        </select>
                    </label>
                    <label>
                        Hashtag <input id='hashtag' type="text" placeholder="Hashtag" onChange={e => this.onChange(e)} />
                    </label>
                    <label>
                        Set name <input id='setname' type="text" placeholder="Set Name" onChange={e => this.onChange(e)} />
                    </label>
                    <button disabled={this.state.isSubmitting}>Create new set</button>
                </form>
                <button onClick={this.onGoBack}>Go back</button>
            </div>
        )
    }

    render() {
        console.log("Render");
        return (
            <div className="manage">
                <p>{this.state.isNewSet ? "true" : "false"}</p>
                {this.state.isNewSet ? this.renderNewSet() : this.renderSetList() }
                
            </div>
        )
    }
}
/* Search string example. Find a way to put this into api form and returns json
 https://twitter.com/search?q=(%23callidrip)%20(from%3AwatsonameliaEN%2C%20OR%20from%3Amoricalliope)&src=typed_query */
export default Manage