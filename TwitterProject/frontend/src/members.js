import React from 'react';
import './members.css';

const Members = props => {

    const onSubmit = (event) => {
        event.preventDefault();
        props.onFormSubmit();
    }

    return (
        <form method='POST' action='http://localhost:3001/members/add' onSubmit={onSubmit} className='MemberForm'>
            <label>
                Member ID: <input id='memberid' type="text" placeholder="MemberID" onChange={e => props.onChange(e)} />
            </label>
            <label>
                Member Name: <input id='name' type="text" placeholder="Member Name" onChange={e => props.onChange(e)} />
            </label>
            <label>
                Image: <input id='image' type="file" placeholder="Member Image" onChange={e => props.onChange(e)}/>
            </label>
            <label>
                Fan Mark: <input id='fanmark' type="text" onChange={e => props.onChange(e)} />
            </label>
            <button disabled={props.isSubmitting}>
                Create Member
            </button>
        </form>
    );
};

export default Members