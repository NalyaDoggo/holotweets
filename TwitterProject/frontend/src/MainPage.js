import React from 'react';
import {BrowserRouter as Router, Link, Switch, Route } from 'react-router-dom';
import Manage from './manage';

const routes = [
    {
      path: "/",
      exact: true,
      topbar: () => <div>Home</div>,
      main: () => <h2>Home</h2>
    },
    {
      path: "/manage",
      exact: true,
      topbar: () => <div>Manage Page</div>,
      main: () => <Manage/>
    },
    {
      path: "/addmembers",
      exact: true,
      topbar: () => <div>Members Page</div>,
      main: () => <p>Members go here</p>
    }
  ];

const MainPage = props => {


    return (
        <div className="MainPage">
            <Router>
                <div className="NavBar">
                    <ul>
                        <li><Link to="/">Home</Link></li>
                        <li><Link to="/manage">Manage Groups</Link></li>
                        <li><Link to="/addmembers">Members</Link></li>
                    </ul>
                </div>
                <Switch>
                {routes.map((route, index) => (
                    <Route 
                    key={index}
                    path={route.path}
                    exact={route.exact}
                    children={<route.topbar/>}
                    />
                ))}
                </Switch>
                <div className="MainBody">
                    <Switch>
                        {routes.map((route, index) => (
                            <Route 
                            key={index}
                            path={route.path}
                            exact={route.exact}
                            children={<route.main/>}
                            />
                        ))}
                    </Switch>
                </div>
                
            </Router>
        </div>
    )
}

export default MainPage