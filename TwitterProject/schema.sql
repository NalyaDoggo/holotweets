CREATE DATABASE holotweetsdb;

use holotweetsdb;

ALTER DATABASE holotweetsdb CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;


/* TODO: Convert the image column in this table to imagepath */
CREATE TABLE IF NOT EXISTS `members` (
`memberid` NVARCHAR(255) NOT NULL,
`name` VARCHAR(50) NOT NULL,
`image` varbinary(6500) NOT NULL,
`fanmark` NVARCHAR(5) NULL,
`isactive` BOOL DEFAULT True
);

ALTER TABLE `members` ADD PRIMARY KEY (`memberid`);
ALTER TABLE `members` MODIFY `memberid` NVARCHAR(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `members` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE `members` MODIFY fanmark TEXT CHARSET utf8mb4;


CREATE TABLE IF NOT EXISTS `membergroupings` (
`membergroupid` INT NOT NULL,
`member` NVARCHAR(255) NOT NULL
);

ALTER TABLE `membergroupings` ADD PRIMARY KEY (`membergroupid`, `member`);
ALTER TABLE `membergroupings` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE `membergroupings` ADD FOREIGN KEY (`member`) REFERENCES members(memberid);
ALTER TABLE `membergroupings` MODIFY `membergroupid` int(11) NOT NULL AUTO_INCREMENT;


CREATE TABLE IF NOT EXISTS `memberset` (
`membersetid` INT NOT NULL,
`membergroupid` INT NOT NULL,
`membersetname` VARCHAR(50) NOT NULL
);

ALTER TABLE `memberset` ADD PRIMARY KEY (`membersetid`);
ALTER TABLE `memberset` MODIFY `membersetid` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `memberset` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE `memberset` ADD FOREIGN KEY (`membergroupid`) REFERENCES membergroupings(membergroupid);

CREATE TABLE IF NOT EXISTS `sets` (
`setid` INT NOT NULL,
`membersetid` INT NOT NULL,
`hashtag` VARCHAR(20) NOT NULL,
`setname` VARCHAR(50) NOT NULL,
`collectionid` int NULL
);

ALTER TABLE `sets` ADD PRIMARY KEY (`setid`);
ALTER TABLE `sets` MODIFY `setid` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `sets` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE `sets` ADD FOREIGN KEY (`membersetid`) REFERENCES memberset(membersetid);

/* 
For data, start by using the page and adding members 
For the images, they're in /backend/images
(1063337246231687169, Ookami Mio, ookamimio.jpg),
(1255013740799356929, Yukihana Lamy, yukihanalamy.jpg, ☃)
(1283646922406760448, Takanashi Kiara, takanashikiara.jpg)
This must be done before the creation of the rest of the data
*/

/* memberid is obtained by using GET at https://api.twitter.com/2/users/by/username/:username
The username being whatever their twitter handle is. @ookamimio for example
*/

/* 
Due to pages not working, please run the following INSERT statements after the above members have been added

INSERT INTO membergroupings (membergroupid, member) VALUES (1, 1255013740799356929), (1, 1063337246231687169)
INSERT INTO memberset (membersetid, membergroupid, membersetname) VALUES (1, 1, "NeeSanGumi")
INSERT INTO sets (setid, membersetid, hashtag, setname, collectionid) VALUES (1, 1, "#ホロのえかきうた", "HoloDraw", null)

*/

